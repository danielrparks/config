These are my configuration files.

They are licensed under the [MIT license](LICENSE), except where otherwise noted or where the original source for a file is not compatible with the MIT license.

Notably, commercial reuse is permitted.
