#!/usr/bin/bash

PID=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true).pid')
kill $PID
sleep 2
kill -9 $PID || true
