LINK=enp193s0f3u2i1
if [[ "$(id -u)" -ne 0 ]]; then
	exec sudo "$(realpath "$BASH_SOURCE")" $@
fi
resolvectl dns $LINK 2606:4700:4700::1111 2606:4700:4700::1001 1.1.1.1 1.0.0.1
# sysctl net.ipv6.conf.$LINK.hop_limit=65
sysctl net.ipv6.conf.$LINK.disable_ipv6=1
sysctl net.ipv4.ip_default_ttl=129
