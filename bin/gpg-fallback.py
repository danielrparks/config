#!/usr/bin/env python3
import os
import sys

env = os.environ
env["PINENTRY_USER_DATA"] = "gtk2_fallback"
os.execve("/usr/bin/gpg", ["gpg"] + sys.argv[1:], env)
