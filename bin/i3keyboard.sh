#!/bin/bash

{
	setxkbmap -layout dvorak
	setxkbmap -option compose:ralt # this has to come first, else it will overwrite custom mappings
	xmodmap -e "clear lock" #disable caps lock switch
	xmodmap -e "keysym Caps_Lock = Escape" #set caps_lock as escape
}# > /home/daniel/bin/i3keyboard.sh.log
